\documentclass{article}

\usepackage[margin=1.25in]{geometry}

\usepackage{fontspec}
\setmainfont[Ligatures=TeX,Scale=1.165]{Latin Modern Roman}
\setmonofont{Monaco}
\usepackage[OT1,EU1]{fontenc}

\usepackage{textcomp}

\usepackage{setspace}
\onehalfspacing

\usepackage{listings}

\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.8}

\usepackage{algorithmicx}

\title{Testing the efficacy of a generic work queue for parallel sorting and matrix multiplication}
\author{Michael Victor Zink \\ Final Project, CMSC 22200, Chien}
\date{\today}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily}

\maketitle

\section{Overview}
\label{sec:overview}

We explore the efficacy of a simple generic work queue for parallelizing two simple example problems: matrix multiplication and sorting an array of integers. We compare the work queue implementation to simple, naive implementations of both problems. We were motivated by the question of whether even a simple, generic work queue could improve performance through parallelization while avoiding fine-tuned parallelization of specific algorithms. To this end, our work queue implementation is compiled and linked identically for both our parallel sorting and parallel matrix multiplication implementations.

This approach is supposed to be an alternative to both higher-level and lower-level approaches such as OpenMP or explicit threading. The programmer doesn't have to write loops in accordance with OpenMP's constraints, nor explicitly manage threads. Instead, they initialize a work queue (which creates a thread pool) and dispatch work units.\footnote{As explained later, a work unit is an arbitrary function pointer and argument pointer --- this is where ``generic'' in ``generic work queue'' comes from.} However, can a simple implementation of a work queue lead to substantial performance increases? For example, this approach does not encourage the programmer to improve data locality with respect to multiple threads.\footnote{For this reason, we see many of the same issues that come up with, for example, dynamic versus static scheduling in OpenMP parallelization: since the simple work queue has no intelligent scheduling approach, and indeed allows work to come in at any time, it is easy for a thread to pick up work which would be performed faster on another thread, for example due to data locality.} In Section~\ref{sec:implementation}, we explain our work queue implementation and how we arrived at it.

The original sorting implementation is a single-threaded bubble sort. In order to parallelize it, we made an important algorithm change: our implementation is essentially a hybrid merged-bubble sort. Given a block size $B$ for an array of length $S$, the algorithm sorts $S/B$ groups of $B$ integers using bubble sort (identical to the original implementation). The results are then sorted using merge sort. Thus, $B=1$ is equivalent to merge sort and $B=S$ is equivalent to bubble sort. We parameterize $B$ in our implementation so that we can see the effect of the algorithm change on overall performance. Since bubble sort is $O\left(n^2\right)$ and merge sort is $O\left(n \log{n}\right)$, this effect is important. However, it is impossible to isolate this effect from that of parallelization, since this implementation of bubble sort is not parallelizable. That is, $N$ threads can sort $N$ different blocks of the input using bubble sort, but those blocks reach their final position through merge sort.

Similarly with matrix multiplication, we made an important algorithm change that has performance benefits before parallelization occurs. The simplest loop reordering greatly improves performance, but it furthermore makes parallelization much easier. In fact, our implementation stores/interprets the right-hand matrix as column-major, instead of row-major. This is identical to loop reordering, but allows us to pass simple row and column pointers into the work queue, so that work threads don't even need to know which row or column they are looking at.\footnote{Note that in the original implementation, the matrix $A$ is generated such that $A^T=A$. We wished to make more general performance improvements, so we did not exploit this fact: we allocate two separate matrices on the heap, instead of only one on the stack.} It was much easier in this case to isolate the effect: since we also wanted to allocate the matrix on the heap instead of the stack, we created a third implementation, {\tt mmult\_malloc}, which more closely mirrors the original implementation, but (1) allocates both the left and right matrix separately, as does our parallel implementation, and (2) uses a column-major matrix on the right hand. As such, out of fairness, most of our experimental results focus on the comparison between our parallel implementation and {\tt mmult\_malloc}, rather than the original implementation.

\section{Work queue implementation}
\label{sec:implementation}

Our work queue implementation consists of three public procedures (make work pool, enqueue work, and finish), one globally shared data object,\footnote{To clarify, the {\tt work\_pool\_t} data object is shared between the worker threads and the dispatch thread, but is not a global variable. Furthermore, we distinguish this data object as ``globally shared'', but each worker thread also shares its queue with the dispatching thread.} and one Lamport queue for each worker thread. Once the threads are created, the dispatch thread (in our tests, the main thread) creates work units and enqueues each in one of the worker threads' queues. It then sets the {\tt finished} flag and waits for all the worker threads to join. The worker threads simply wait for work to show up in their queue, run the function prescribed by the work unit with the arguments prescribed by the work unit, and repeat. If a worker thread's queue is empty and the finished flag is set, the thread terminates. For illustration, a few procedures from our implementation are included in Figure~\ref{fig:enq}.

\begin{figure}[htdp]
\begin{center}
\begin{lstlisting}[fontadjust]
int q_enq(q_t *q, volatile void *item) {
    if ((q->tail - q->head) == q->depth)
        return Q_FULL;
    q->items[(q->tail++) % q->depth] = item;
    return Q_OK;
}
void * work_pool_worker(void *vargs) {
    worker_args_t *args = (worker_args_t *) vargs;
    work_unit_t * unit;
    int res = -1;
    while ((res = work_pool_deq(args->wp, args->id,
    	&unit)) == WP_OK) {
        	unit->f(unit->args);
    }
    return NULL;
}
\end{lstlisting}
\end{center}
\caption{The {\tt enq} and {\tt worker} procedures for our wait-free queue.}
\label{fig:enq}
\end{figure}


The Lamport queue is the most significant design decision, and is closely tied to most of the tradeoffs and compromises in our implementation. A Lamport queue, or wait-free queue, is a lock-free, single-enqueuer, single-dequeuer FIFO queue. Each of these attributes has pros and cons for our implementation.

We investigated other alternatives, but no lock-free versions were appropriate for our application. Especially interesting was David's single-enqueuer, multiple-dequeuer wait free queue \cite{david04}, but this algorithm requires unbounded space for its $O(1)$ enqueue and dequeue operations. If the space is bounded, it becomes $O(n)$ where $n$ is the number of threads. This may still have yielded better performance, but would have required much more effort to implement. Another alternative, discovered too late to implement, was the lock free Fast-Forward Queue \cite{Giacomoni08fastforwardfor}.

Our first iteration, to ensure correctness before we moved on to performance, used a single queue with a single mutex: the dispatch thread would acquire the lock, enqueue an item, and release the lock. Similarly, each worker thread would acquire the lock, dequeue an item, and release the lock. This allowed us to test our implementation, but the overhead was so high that using one worker thread had much worse performance than the original implementation, for both sorting and matrix multiplication. Furthermore, that performance only got worse when more worker threads were added. Avoiding mutexes was key to minimizing the overhead of our work queue. As such, we gave each worker thread its own queue, so that each queue would remain single-enqueuer, single-dequeuer, but the dispatch thread would cycle between the different workers.

We had a serious gotcha when we realized that the {\tt work\_unit\_t} struct used to tell worker threads what to do was not getting updated in the worker thread's caches by the time they tried to execute them. Since the queue only stores pointers to work units, we had to add a call to {\tt \_\_sync\_synchronize() } in the enqueue procedure. By creating a memory barrier, we probably suffered a performance hit compared to an ideal world where cache coherency isn't an issue, but it ensures correctness and prevents segmentation faults. Since this problem prevented correct operation, we couldn't collect data to determine the performance hit of having a memory fence at every (successful) enqueue.

The other significant pitfall with the lock-free queue approach is that it entails a lot of spinning. That is, if the dispatch thread can't find anywhere to put a work unit, it will spin until one of the worker threads dequeues another item, trying to enqueue it on every worker. Similarly, if there is no work in a particular worker's queue, it will spin until either the global {\tt finished} flag is set or the dispatch thread gives it more work. This means that even in the start-up and slow-down periods, where work is being created, or only a few work units remain, all worker threads could be spinning at near-100\% CPU usage until the caches on their core reflect the updated {\tt finished} state.

This aspect is not explored deeply in our experiments, but each queue has a depth $D$, the maximum number of items in can hold before enqueue attempts return {\tt Q\_FULL}. This was parameterized in our implementation, and all tests were run with several values for $D$. Again, we don't delve into this too much, but we found that queue depth had a minimal impact performance for both sorting and matrix multiplication, except that very large depths (64 items) and very small depths (1 item) may not have performed as well, but the data doesn't really say much.

\subsection{Code}

The complete code for this implementation is too long to print out. The above description should be sufficient to understand the basics of the implementation, and the implementation itself is close to the simplest that could be produced from this explanation. Nonetheless, the code is available in its entirety at https://bitbucket.org/zuwiki/arch-proj

\section{Experiments}
\label{sec:experiments}

We ran tests on a machine with an Intel\textregistered{} Core\texttrademark{} i7-3770 processor (8MB cache, 3.4-3.9 GHz, 4 cores with Hyper-Threading\texttrademark) and 16 GB of main memory. For simplicity, we only look at performance in terms of real time.

When comparing sorting implementations, we first looked at larger values of $B$. As described in Section~\ref{sec:overview}, this means blocks of size $B$ are sorted with bubble sort before being merged in to final position. Table~\ref{tab:merge_speedup} shows the runtimes for various $N$ and $S$. The performance of the baseline (unparallelized) version compared to any number of cores with $S=1000$ shows the overhead of the work queue approach. In that scenario, the parallel version is essentially running one bubble sort. However, the speedup is obvious when looking at $S=10000$ and $S=100000$.

\begin{table}[htdp]
\begin{center}
\begin{tabular}{c|c|c|c}
$N$	&	$S=1000$	&	$S=10000$	&	$S=100000$ \\
\hline
baseline	&	1.2098	&	51.5288	&	4672.35	\\
1	&	1.6798	&	10.4702		&	54.691	\\
2   	&	1.6588	&	5.4248		&	35.1474	\\
3	&	1.65		&	5.3166		&	30.3668	\\
4	&	1.6434	&	4.0756		&	27.016	\\
6	&	1.6654	&	3.3688		&	23.3574	\\
8	&	1.6762	&	2.998		&	49.5378	\\
12	&	1.6868	&	8.0684		&	201.829	\\
16	&	1.7172	&	7.2404		&	212.127
\end{tabular}
\end{center}
\caption{Parallel runtime (ms) for various $N$ and $S$ for $B=1000$, with baseline runtime.}
\label{tab:merge_speedup}
\end{table}

We can also look at the effect of different block sizes for large inputs ($S=100000$), as seen in Figure~\ref{fig:blocksize_speedup}. This indicates, amongst the tested values, an optimal block size of around $B=100$. The plot line for $N=1$ also demonstrates the direct slowdown of doing a smaller proportion of the work via merge sort.

\begin{figure}[htdp]
\begin{center}
\begin{tikzpicture}
\begin{axis}[
	xlabel={$B$},
	ylabel={Runtime (ms)},
	legend pos=north west,
	width=4.0in
]

\addplot coordinates{
	(10,9.9572)
	(100,2.4374)
	(250,18.4782)
	(500,29.6902)
	(1000,54.691)
};

\addplot coordinates{
	(10,9.8424)
	(100,12.5508)
	(250,12.028)
	(500,15.052)
	(1000,27.016)
};

\addplot coordinates{
	(10,10.54)
	(100,5.7388)
	(250,7.9456)
	(500,14.0794)
	(1000,23.3574)
};

\legend{$N=1$,$N=4$,$N=6$}
\end{axis}
\end{tikzpicture}
\end{center}
\caption{Runtime for various $B$ and $N$ at $S=100000$. Baseline runtime was 4672.35 ms.}
\label{fig:blocksize_speedup}
\end{figure}

To demonstrate the difference in the algorithm change between the baseline matrix multiplication and our serial modification, Table~\ref{tab:mmult_malloc_diff} shows their runtimes at various sizes.

\begin{table}[htdp]
\begin{center}
\begin{tabular}{c|c|c|c}
$S$	&	Baseline	&	Improved	&	$\frac{Baseline}{Improved}$	\\
\hline
10	&	0.0011	&	0.0018	&	1.64	\\
100	&	0.8547	&	0.8082	&	0.95	\\
1000	&	1467.44	&	308.847	&	0.21	\\
5000	&	segfault	&	41604.6	&	-
\end{tabular}
\end{center}
\caption{Improved serial matrix multiplication for various $S$.}
\label{tab:mmult_malloc_diff}
\end{table}

The difference in performance with different $N$ can be seen in Figure~\ref{fig:mmult1000}. This is our best indicator of how much of a benefit we can actually get from parallel processing for this example. Our improved serial version, with effectively no more improvement than a loop reordering, outperforms our serial implementation for most values of $N$ when $S=1000$. Nonetheless, we can see that at times, there is a clear performance gain even with our simplistic work queue. Furthermore, for much larger inputs, such as at $S=5000$ seen in Figure~\ref{fig:mmult5000}, the parallel version consistently outperformed the improved serial version.

\begin{figure}[htdp]
\begin{center}
\begin{tikzpicture}
\begin{axis}[
	xlabel={$N$},
	ylabel={Parallel runtime versus improved serial},
	legend pos=north west,
	width=4.0in
]

\addplot coordinates{
	(1,1.25881)
	(2,0.670635)
	(3,0.466977)
	(4,0.56302)
	(6,0.558067)
	(8,0.873548)
	(12,1.14748)
	(16,1.31526)
};

\end{axis}
\end{tikzpicture}
\end{center}
\caption{Matrix multiplication: runtime of parallel for various $N$ versus the improved serial version at $S=1000$.}
\label{fig:mmult1000}
\end{figure}

\begin{figure}[htdp]
\begin{center}
\begin{tikzpicture}
\begin{axis}[
	xlabel={$N$},
	ylabel={Parallel runtime versus improved serial},
	legend pos=north west,
	width=4.0in
]

\addplot coordinates{
(1,1.07015)
(2,0.595864)
(3,0.562575)
(4,0.561328)
(6,0.558079)
(8,0.563044)
(12,0.563871)
(16,0.56629)
};

\end{axis}
\end{tikzpicture}
\end{center}
\caption{Matrix multiplication: runtime of parallel for various $N$ versus the improved serial version at $S=5000$.}
\label{fig:mmult5000}
\end{figure}

\section{Conclusion}

The fact that most of our tests showed peak performance with number of threads equal to, or just under, the number of cores indicates that more performance would be gained with more cores. Amdahl's law says that a substantial portion of the computation is indeed parallel, and the serial portion (in other words, our work queue implementation) would be no more significant if more cores, and thus more threads, were added. That is, our implementation becomes a bottleneck as soon as $n$ surpasses the number of available cores, but the speedup, for these applications, would still be substantial for large inputs if more cores were added.

\newpage

\bibliographystyle{unsrt}
\bibliography{bibliography}

\end{document}