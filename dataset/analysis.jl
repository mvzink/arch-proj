using DataFrames

# Produce arithmetic means, standard deviation, and error bar mins/maxes for time
meansExpr = :(Tmean = mean(Time); Tstdev = std(Time); Tmin = mean(Time) - std(Time); Tmax = mean(Time) + std(Time))

# Sorting data
bsort_baseline = readtable("bsort_baseline.csv")
merge_q = readtable("merge_q.csv")

bsort_baseline_by_size = by(bsort_baseline, "Size", meansExpr)
show(bsort_baseline_by_size)
bsort_merge_q_by_threads_and_size = by(merge_q, ["Nthreads", "Size"], meansExpr)
show(bsort_merge_q_by_threads_and_size)

# Matrix multiplication data
mmult_baseline = readtable("mmult_baseline.csv")
mmult_malloc = readtable("mmult_malloc.csv")
mmult_q = readtable("mmult_q.csv")

mmult_q_by_threads_and_size = by(mmult_q, ["Nthreads", "Size"], meansExpr)
show(mmult_q_by_threads_and_size)
mmult_baseline_by_size = by(mmult_baseline, "Size", meansExpr)
show(mmult_baseline_by_size)
mmult_malloc_by_size = by(mmult_malloc, "Size", meansExpr)
show(mmult_malloc_by_size)


# fontspec = "serif"
# theme = Theme(minor_label_font=fontspec,
    # major_label_font=fontspec,
    # point_label_font=fontspec)

# fig1_mmult_size1000 = plot(
    # mmult_q_by_threads_and_size[:(Size .== 1000), :], x="Nthreads",
    # y="Tmean", ymin="Tmin", ymax="Tmax",
    # yintercept=[mmult_malloc_by_size[:(Size .== 1000), :Tmean], mmult_baseline_by_size[:(Size .== 1000), :Tmean]],
    # Geom.point, Geom.errorbar, Geom.line, Geom.hline,
    # Scale.x_discrete, Scale.y_continuous(format=:plain, maxvalue=mmult_baseline_by_size[:(Size .== 1000), :Tmax][1]), theme)
# draw(PDF("fig1_mmult_size1000.pdf", 3inch, 3inch), fig1_mmult_size1000)

# fig2_mmult_size5000 = plot(
    # mmult_q_by_threads_and_size[:(Size .== 5000), :], x="Nthreads",
    # y="Tmean", ymin="Tmin", ymax="Tmax",
    # yintercept=[mmult_malloc_by_size[:(Size .== 5000), :Tmean]],
    # Geom.point, Geom.errorbar, Geom.line, Geom.hline,
    # Scale.x_discrete, Scale.y_continuous(format=:plain), theme)
# draw(PDF("fig2_mmult_size5000.pdf", 3inch, 3inch), fig2_mmult_size5000)

nothing
