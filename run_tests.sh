mkdir -p tests
mkdir -p tests/`date +%Y-%m-%d-%H%M` && cd tests/`date +%Y-%m-%d-%H%M`

echo mmult_baseline
for size in {TINY,SMALL,MEDIUM}; do
	for trial in {1..5}; do
		../../build/mmult_baseline-$size >> mmult_baseline.csv
		printf .
	done
done

echo
echo mmult_malloc
for size in {10,100,1000,5000}; do
	for trial in {1..5}; do
		../../build/mmult_malloc $size >> mmult_malloc.csv
		printf .
	done
done

echo
echo mmult_q
for nthreads in {1,2,3,4,6,8,12,16}; do
	for depth in {1,4,8,16,32,64}; do
		for size in {10,100,1000,5000}; do
			for trial in {1..5}; do
				../../build/mmult_q $nthreads $depth $size >> mmult_q.csv
				printf .
			done
		done
	done
done

echo
echo bsort_baseline
for size in {SMALL,MEDIUM,LARGE,XLARGE}; do
	for trial in {1..5}; do
		../../build/bsort_baseline-$size >> bsort_baseline.csv
		printf .
	done
done

echo
echo merge_q
for nthreads in {1,2,3,4,6,8,12,16}; do
	for depth in {1,4,8,16,32,64}; do
		for size in {1000,10000,100000}; do
			for blocksize in {10,100,250,500,1000,1000000}; do
				for trial in {1..5}; do
					../../build/merge_q $nthreads $depth $size $blocksize >> merge_q.csv
					printf .
				done
			done
		done
	done
done

