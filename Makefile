tests: progs
	./run_tests.sh

progs:
	make -C src mmult_malloc merge_q mmult_q
	SIZE=TINY make -C src mmult_baseline
	SIZE=SMALL make -C src bsort_baseline mmult_baseline
	SIZE=MEDIUM make -C src bsort_baseline mmult_baseline
	SIZE=LARGE make -C src bsort_baseline mmult_baseline
	SIZE=XLARGE make -C src bsort_baseline

