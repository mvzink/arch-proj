/* merged-bubble sort with work queue */

#include <stdio.h>
#include <pthread.h>
#include <assert.h>

#include "util/stopwatch.h"
#include "util/work_pool.h"
#include "util/util.h"

typedef struct args {
    volatile int done;
} args_t;

typedef struct bsort_args {
    args_t args;
    int *array;
    int start, end;
} bsort_args_t;

typedef struct merge_args {
    args_t args;
    int *array;
    int left_start, right_start, end;
    args_t *left;
    args_t *right;
} merge_args_t;

int merge(void * vargs) {
    merge_args_t *args = (merge_args_t *) vargs;
    int *a = args->array;
    int left_start = args->left_start;
    int right_start = args->right_start;
    int end = args->end;
    int b[right_start - left_start];
    while (!args->left->done) {};
    for (int c = 0; c < (right_start - left_start); c++) {
        b[c] = a[c];
    }
    while (!args->right->done) {};
    int i = 0;
    int j = right_start;
    int k = left_start;
    int m = right_start - left_start;
    while ((i < m) && (j <= end)) {
        a[k++] = (a[j] < b[i]) ? a[j++] : b[i++];
    }
    while (i < m) {
        a[k++] = b[i++];
    }
    free(args->left);
    free(args->right);
    for (int c = left_start; c < end; c++ )
        if (a[c] > a[c+1]) eprintf("merge: whoops: out of order at %d\n", c);
    args->args.done = 1;
    return 0;
}

int bsort(void * vargs) {
    bsort_args_t *args = (bsort_args_t *) vargs;
    int *array = args->array;
    int start = args->start;
    int end = args->end;
    int swap;
    for (int c = start; c < end; c++) {
        for (int d = 0; d < (end - c); d++) {
            if (array[start+d] > array[start+d+1]) {
                swap = array[start+d];
                array[start+d] = array[start+d+1];
                array[start+d+1] = swap;
            }
        }
    }
    for (int c = start; c < end; c++ )
        if (array[c] > array[c+1]) eprintf("bsort: whoops: out of order at %d\n", c);
    args->args.done = 1;
    return 0;
}

args_t * dispatch_work(work_pool_t *wp, int array[], int start, int end, int blocksize) {
#ifdef DEBUG
    static int global_debug_i = 0;
    static int global_unit_i = 0;
    int debug_i = global_debug_i++;
#endif
    if ((end - start) <= blocksize) {
        eprintf("%d: making bsort worker for %d-%d\n", debug_i, start, end);
        bsort_args_t *r = (bsort_args_t *) malloc(sizeof(bsort_args_t));
        eprintf("%d: allocated bsort worker at %lx\n", debug_i, (unsigned long) r);
        if (r == NULL) {
            eprintf("%d: out of memory making bsort work unit\n", debug_i);
            exit(1);
        }
        r->args.done = 0;
        r->array = array;
        r->start = start;
        r->end = end;
        eprintf("%d: sending bsort unit %d\n", debug_i, global_unit_i++);
        work_pool_enq(wp, bsort, (void *) r);
        return (args_t *) r;
    } else {
        eprintf("%d: making merge worker for %d-%d\n", debug_i, start, end);
        int m = (start + end) >> 1;
        args_t * left_args = dispatch_work(wp, array, start, m, blocksize);
        args_t * right_args = dispatch_work(wp, array, m+1, end, blocksize);
        merge_args_t *r = (merge_args_t *) malloc(sizeof(merge_args_t));
        eprintf("%d: allocated merge worker at %lx\n", debug_i, (unsigned long) r);
        if (r == NULL) {
            eprintf("%d: out of memory making merge work unit\n", debug_i);
            exit(1);
        }
        r->args.done = 0;
        r->array = array;
        r->left_start = start;
        r->right_start = m+1;
        r->end = end;
        r->left = left_args;
        r->right = right_args;
        eprintf("%d: sending merge unit %d\n", debug_i, global_unit_i++);
        work_pool_enq(wp, merge, (void *) r);
        return (args_t *) r;
    }
    // trololol
    exit(1);
    return NULL;
}

int main(int argc, char * argv[]) {

    if (argc >= 5) {
        // Get number of threads
        const int nthreads = strtol(argv[1], NULL, 10);
        const size_t depth = strtol(argv[2], NULL, 10);
        const int n = strtol(argv[3], NULL, 10);
        const int blocksize = strtol(argv[4], NULL, 10);
        // these assertions represent assumptions which could easily be
        // eliminated around with a little more care, but aren't worth the
        // effort (in terms of testing) for our purposes
        /* assert((n % 2) == 0);
        assert((blocksize % 2) == 0);
        assert((n % blocksize) == 0); */

        int *array = (int *) malloc(n * sizeof(int));
        int c;

        for (c = 0; c < n; c++){
            array[c] = (c << 6) % (17*17); 
        }

        work_pool_t *wp;
        int res = work_pool_make(nthreads, depth, &wp);

        stopwatch_t stopwatch;
        startStopwatch(&stopwatch);

        args_t *root = dispatch_work(wp, array, 0, n-1, blocksize);

        work_pool_finish(wp);

        stopStopwatch(&stopwatch);
        printf("%d,%lu,%d,%d,%f\n", nthreads, depth, n, blocksize, getElapsedTime(&stopwatch));

        free(root);

        for ( c = 0 ; c < n-1 ; c++ )
            if (array[c] > array[c+1]) printf("Whoops: error at position %d\n", c);
    }

    return 0;
}

