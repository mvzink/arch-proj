/* bubble sort */

#include <stdio.h>

#include "util/stopwatch.h"

#ifdef ARY_SMALL
#define ARY_SIZE 1000 // 1K
#endif

#ifdef ARY_MEDIUM
#define ARY_SIZE  10000 // 10K
#endif

#ifdef ARY_LARGE
#define ARY_SIZE  100000 // 100k
#endif

#ifdef ARY_XLARGE
#define ARY_SIZE  1000000 // 1M
#endif

int main() {
    int n = ARY_SIZE;
    int c, d, swap;
    int array[n];

    for (c = 0; c < n; c++){
        array[c] = (c << 6) % (17*17); 
    }

    stopwatch_t stopwatch;
    startStopwatch(&stopwatch);

    for (c = 0 ; c < ( n - 1 ); c++) {
        for (d = 0 ; d < n - c - 1; d++) {
            if (array[d] > array[d+1]) { /* For decreasing order use < */
                swap = array[d];
                array[d] = array[d+1];
                array[d+1] = swap;
            }
        }
    }

    stopStopwatch(&stopwatch);
    printf("%d,%f\n", n, getElapsedTime(&stopwatch));

    for ( c = 0 ; c < n-1 ; c++ )
        if (array[c] > array[c+1]) printf("Whoops: error at position %d\n", c);

    return 0;
}

