/* matrix multiplication */

#include <stdio.h>
#include <stdlib.h>

#include "util/stopwatch.h"
#include "util/util.h"

#ifdef MAT_TINY
#define MAT_SIZE 10 // 10x10
#endif

#ifdef MAT_SMALL
#define MAT_SIZE  100 // 100x100
#endif

#ifdef MAT_MEDIUM
#define MAT_SIZE 1000 // 1000x1000
#endif

#ifdef MAT_LARGE
#define MAT_SIZE 5000 // 5000x5000
#endif

int main(int argc, char * argv[]) {
    if (argc >= 2) {
        const int size = strtol(argv[1], NULL, 10);

        // left is row-major
        // right is column-major
        int **left = (int **) calloc(size, sizeof(int *));
        int **right = (int **) calloc(size, sizeof(int *));
        for (int c = 0; c < size; c++) {
            left[c] = (int *) calloc(size, sizeof(int));
            right[c] = (int *) calloc(size, sizeof(int));
            for (int d = 0; d < size; d++) {
                left[c][d] = ((c+d) % 10) - 1;
                right[c][d] = ((c+d) % 10) - 1;
            }
        }
#ifdef DEBUG
        printf("left:\n");
        print_matrix(size, left);
        printf("right:\n");
        print_matrix(size, right);
#endif

        int **out = (int **) calloc(size, sizeof(int *));
        for (int c = 0; c < size; c++) {
            out[c] = (int *) calloc(size, sizeof(int));
        }

        stopwatch_t stopwatch;
        startStopwatch(&stopwatch);

        int sum;
        for (int c = 0; c < size; c++) {
            for (int d = 0; d < size; d++) {
                for (int k = 0; k < size; k++) {
                    sum += left[c][k] * right[d][k];
                }

                out[c][d] = sum;
                sum = 0;
            }
        }

        stopStopwatch(&stopwatch);
        printf("%d,%f\n", size, getElapsedTime(&stopwatch));

        /* This is just so that -O3 won't optimize out the entire matrix multiplication */
#ifdef DEBUG
        printf("out:\n");
        print_matrix(size, out);
#else
        for (int c = 0; c < size; c++) {
            for (int d = 0; d < size; d++) {
                if (out[c][d] > 0) {
                    exit(0);
                }
            }
        }
#endif
    }

    return 0;
}

