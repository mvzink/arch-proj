/* matrix multiplication */

#include <stdio.h>

#include "util/stopwatch.h"

#ifdef MAT_TINY
#define MAT_SIZE 10 // 10x10
#endif

#ifdef MAT_SMALL
#define MAT_SIZE  100 // 100x100
#endif

#ifdef MAT_MEDIUM
#define MAT_SIZE 1000 // 1000x1000
#endif

#ifdef MAT_LARGE
#define MAT_SIZE 5000 // 5000x5000
#endif

int main() {
    int c, d, k, sum = 0;

    int size = MAT_SIZE;
    int first[size][size], multiply[size][size];

    for (c = 0; c < size; c++)
        for (d = 0; d < size; d++)
            first[c][d] = ((c+d) % 2) - 1;

    stopwatch_t stopwatch;
    startStopwatch(&stopwatch);

    for (c = 0; c < size; c++) {
        for (d = 0; d < size; d++) {
            for (k = 0; k < size; k++) {
                sum += first[c][k] * first[k][d];
            }

            multiply[c][d] = sum;
            sum = 0;
        }
    }

    stopStopwatch(&stopwatch);
    printf("%d,%f\n", size, getElapsedTime(&stopwatch));

    /* This is just so that -O3 won't optimize out the entire matrix multiplication */
    for (c = 0; c < size; c++) {
        for (d = 0; d < size; d++) {
            if (multiply[c][d] > 9) {
                return 0;
            }
        }
    }

    return 0;
}

