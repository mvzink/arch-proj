#include "stopwatch.h"
#include <stdlib.h>
#include <stdio.h>

void startStopwatch(stopwatch_t *watch) {
        gettimeofday(&watch->startTime, NULL);
}

void stopStopwatch(stopwatch_t *watch) {
        gettimeofday(&watch->stopTime, NULL);
}

double getElapsedTime(stopwatch_t *watch) {
        return ((watch->stopTime.tv_sec-watch->startTime.tv_sec)*1000000LL + watch->stopTime.tv_usec-watch->startTime.tv_usec)/1000.0f;
}

