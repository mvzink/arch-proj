#ifndef UTIL_H_
#define UTIL_H_

#ifdef DEBUG
#define eprintf(args...) \
    printf(args)
#else
#define eprintf(args...) \
    do {} while (0)
#endif

void print_matrix(int size, int ** mat);

#endif /* UTIL_H_ */
