#include "util.h"

#include <stdio.h>

void print_matrix(int size, int ** mat) {
    for (int c = 0; c < size; c++) {
        printf("[ ");
        for (int d = 0; d < size; d++) {
            printf("%d ", mat[c][d]);
        }
        printf("]\n");
    }
}


