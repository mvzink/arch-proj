#ifndef Q_H_
#define Q_H_

/*
 * A simple lock-free queue
 * Michael Victor Zink
 */

#include <stdlib.h>

/* Return and error codes: */
#define Q_OK (0)       /* Operation successful */
#define Q_ENOMEM (1)   /* Not enough memory */
#define Q_FULL (2)     /* The queue is full */
#define Q_EMPTY (3)    /* The queue is empty */

typedef struct q {
    volatile void ** items;
    volatile int head, tail;
    size_t depth;
} q_t;

/* q_make
 * Allocate and initialize a work queue
 *
 * Params:
 * - depth: the queue depth
 * - q: out-param for newly allocated q
 *
 * Returns:
 * - Q_OK: success
 * - Q_ENOMEM: either the q or the items array couldn't be allocated
 */
int q_make(size_t depth, q_t **q);

/* q_enq
 * Enqueue an item
 *
 * Params:
 * - q: the q to operate on
 * - item: the item to enqueue
 *
 * Returns:
 * - Q_OK: success
 * - Q_FULL: enqueue would have exceeded queue depth
 */
int q_enq(q_t *q, volatile void *item);

/* q_deq
 * Dequeue an item
 *
 * Params:
 * - q: the q to operate on
 * - item: out-param for the returned item
 *
 * Returns:
 * - Q_OK: success
 * - Q_EMPTY: no items in queue
 */
int q_deq(q_t *q, volatile void **item);

#endif /* Q_H_ */
