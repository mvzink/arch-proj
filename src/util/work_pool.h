#ifndef WORK_POOL_H_
#define WORK_POOL_H_

/*
 * A simple queue-based work pool
 * Michael Victor Zink
 */

#include <stdlib.h>
#include <pthread.h>

#include "q.h"

/* Return and error codes: */
#define WP_OK (0)       /* Operation successful */
#define WP_ENOMEM (1)   /* Not enough memory */
#define WP_FULL (2)     /* The queue is full */
#define WP_EMPTY (3)    /* The queue is empty */
#define WP_FINISHED (4) /* The queue is empty and no more will be added */

struct work_pool;

typedef struct worker_args {
    int id;
    struct work_pool *wp;
    q_t *q;
} worker_args_t;

typedef struct work_pool {
    pthread_t *workers;
    worker_args_t *wargs;
    int nthreads;
    int finished;
    int clock;
} work_pool_t;

/* work_pool_make
 * Allocate and spin up a pool of workers
 *
 * Params:
 * - nthreads: number of worker threads
 * - depth: queue depth
 * - wp: out-param for newly allocated work pool
 *
 * Returns:
 * - WP_OK: success
 * - WP_ENOMEM: either the work queue or the workers couldn't be allocated
 */
int work_pool_make(int, int, work_pool_t **);

/* work_pool_enq
 * Send a new work unit to the work queue
 *
 * If the function returns non-zero, it will be re-queued.
 * If the queue is full, blocks until it can be added.
 *
 * Params:
 * - wp: work pool to use
 * - f: a function pointer with integer-return and null-pointer argument
 * - arg: pointer to arguments
 *
 * Returns:
 * - WP_OK: success
 */
int work_pool_enq(work_pool_t *, int (*)(void *), void *);

/* work_pool_finish
 * Flags the work pool as finished and waits for workers to join
 *
 * Once the remaining work is done, workers terminate.
 * Blocks until all workers have terminated.
 *
 * Params:
 * - wp: work pool to finish
 *
 * Returns:
 * - WP_OK: success
 */
int work_pool_finish(work_pool_t *);

#endif /* WORK_POOL_H_ */
