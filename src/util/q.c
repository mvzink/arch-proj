#include "q.h"

/*
 * A simple lock-free queue
 * Michael Victor Zink
 */

int q_make(size_t depth, q_t **q) {
    q_t *w = (q_t *) malloc(sizeof(q_t));
    if (w == NULL)
        return Q_ENOMEM;

    w->items = (volatile void **) malloc(depth * sizeof(volatile void *));
    if (w->items == NULL) {
        free(w);
        return Q_ENOMEM;
    }

    w->depth = depth;
    w->head = 0;
    w->tail = 0;
    *q = w;

    return Q_OK;
}

int q_enq(q_t *q, volatile void *item) {
    if ((q->tail - q->head) == q->depth)
        return Q_FULL;

    q->items[(q->tail++) % q->depth] = item;
    return Q_OK;
}

int q_deq(q_t *q, volatile void **item) {
    if ((q->tail - q->head) == 0)
        return Q_EMPTY;

    *item = q->items[(q->head++) % q->depth];
    return Q_OK;
}

