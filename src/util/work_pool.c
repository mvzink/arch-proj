#include "work_pool.h"

#include <signal.h>
#include <stdio.h>

#include "util.h"

/*
 * A simple queue-based work pool
 * Michael Victor Zink
 */

typedef struct work_unit {
    int (*f)(void*);
    void * args;
#ifdef DEBUG
    int debug_i;
#endif
} work_unit_t;

void * work_pool_worker(void *);

int work_pool_make(int nthreads, int depth, work_pool_t **wp) {
    work_pool_t *p = (work_pool_t *) malloc(sizeof(work_pool_t));
    if (p == NULL) {
        return WP_ENOMEM;
    }

    p->nthreads = nthreads;

    p->workers = (pthread_t *) calloc(p->nthreads, sizeof(pthread_t));
    if (p->workers == NULL) {
        free(p);
        return WP_ENOMEM;
    }

    p->wargs = (worker_args_t *) calloc(p->nthreads, sizeof(worker_args_t));
    for (int i = 0; i < p->nthreads; i++) {
        p->wargs[i].id = i;
        p->wargs[i].wp = p;
        q_make(depth, &p->wargs[i].q);
        pthread_create(&p->workers[i], NULL, &work_pool_worker, (void *) &p->wargs[i]);
        // TODO: check pthread errors
    }

    *wp = p;

    return WP_OK;
}

int work_pool_enq_unit(work_pool_t *wp, work_unit_t *unit) {
    int res = -1;
    while ((res = q_enq(wp->wargs[wp->clock++ % wp->nthreads].q, (void *) unit)) != Q_OK) {
        sched_yield();
    }
    __sync_synchronize();
    return WP_OK;
}

int work_pool_enq(work_pool_t *wp, int (*f)(void *), void * args) {
    static int global_debug_i = 0;
    work_unit_t *unit = (work_unit_t *) malloc(sizeof(work_unit_t));
    unit->f = f;
    unit->args = args;
#ifdef DEBUG
    unit->debug_i = global_debug_i++;
#endif
    eprintf("work_pool_enq: enqueueing work unit %d (%lx : %lx)\n", unit->debug_i, (unsigned long) unit->f, (unsigned long) unit->args);
    return work_pool_enq_unit(wp, unit);
}

int work_pool_deq(work_pool_t *wp, int i, work_unit_t **unit) {
    int res = -1;
    while ((res = q_deq(wp->wargs[i].q, (volatile void **) unit)) != Q_OK) {
        if ((res == Q_EMPTY) && wp->finished) {
            return WP_FINISHED;
        } else {
            sched_yield();
        }
    }
    return WP_OK;
}

int work_pool_finish(work_pool_t *wp) {
    wp->finished = 1;
    for (int i = 0; i < wp->nthreads; i++) {
        pthread_join(wp->workers[i], NULL);
        free(wp->wargs[i].q);
    }
    free(wp->workers);
    free(wp->wargs);
    return WP_OK;
}

void * work_pool_worker(void *vargs) {
    worker_args_t *args = (worker_args_t *) vargs;
    work_unit_t * unit;
    int res = -1;
    while ((res = work_pool_deq(args->wp, args->id, &unit)) == WP_OK) {
        eprintf("thread %d: working on %d\n", args->id, unit->debug_i);
        unit->f(unit->args);
        eprintf("thread %d: finished %d\n", args->id, unit->debug_i, );
    }
    return NULL;
}

