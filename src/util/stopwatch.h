#ifndef STOPWATCH_H_
#define STOPWATCH_H_

/* simple stopwatch convenience functions */

#include <sys/time.h>

typedef struct {
        struct timeval startTime;
        struct timeval stopTime;
} stopwatch_t;

void startStopwatch(stopwatch_t *);

void stopStopwatch(stopwatch_t *);

double getElapsedTime(stopwatch_t *);

#endif /* STOPWATCH_H_ */
