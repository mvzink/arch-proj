/* matrix multiplication with work queue */

#include <stdio.h>
#include <pthread.h>
#include <assert.h>

#include "util/stopwatch.h"
#include "util/work_pool.h"
#include "util/util.h"

typedef struct dp_args {
    int size;
    int *is;
    int *js;
    int *out;
} dp_args_t;

int dp(void * vargs) {
    dp_args_t *args = (dp_args_t *) vargs;
    int size = args->size;
    int *is = args->is;
    int *js = args->js;
    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum += is[i] * js[i];
    }
    *args->out = sum;
    return 0;
}

int main(int argc, char * argv[]) {
    if (argc >= 4) {
        // Get number of threads
        const int nthreads = strtol(argv[1], NULL, 10);
        const size_t depth = strtol(argv[2], NULL, 10);
        const int size = strtol(argv[3], NULL, 10);

        // left is row-major
        // right is column-major
        int **left = (int **) calloc(size, sizeof(int *));
        int **right = (int **) calloc(size, sizeof(int *));
        for (int c = 0; c < size; c++) {
            left[c] = (int *) calloc(size, sizeof(int));
            right[c] = (int *) calloc(size, sizeof(int));
            for (int d = 0; d < size; d++) {
                left[c][d] = ((c+d) % 10) - 1;
                right[c][d] = ((c+d) % 10) - 1;
            }
        }
#ifdef DEBUG
        printf("left:\n");
        print_matrix(size, left);
        printf("right:\n");
        print_matrix(size, right);
#endif

        int **out = (int **) calloc(size, sizeof(int *));
        for (int c = 0; c < size; c++) {
            out[c] = (int *) calloc(size, sizeof(int));
        }

        work_pool_t *wp;
        int res = work_pool_make(nthreads, depth, &wp);

        stopwatch_t stopwatch;
        startStopwatch(&stopwatch);

        for (int c = 0; c < size; c++) {
            for (int d = 0; d < size; d++) {
                dp_args_t * args = (dp_args_t *) malloc(sizeof(dp_args_t));
                args->size = size;
                args->is = left[c];
                args->js = right[d];
                args->out = &out[c][d];
                work_pool_enq(wp, &dp, args);
            }
        }

        work_pool_finish(wp);

        stopStopwatch(&stopwatch);
        printf("%d,%lu,%d,%f\n", nthreads, depth, size, getElapsedTime(&stopwatch));

        /* This is just so that -O3 won't optimize out the entire matrix multiplication */
#ifdef DEBUG
        printf("out:\n");
        print_matrix(size, out);
#else
        for (int c = 0; c < size; c++) {
            for (int d = 0; d < size; d++) {
                if (out[c][d] > 9000) {
                    return 0;
                }
            }
        }
#endif
    }

    return 0;
}

